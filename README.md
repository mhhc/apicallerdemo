# API Caller Demo App #
Demo app for self-learning on Android Jetpack Architecture Component, Retrofit for REST api communication, Glide for image loading, RecyclerView, Coroutine, Espresso for UI tests, etc.

![App Icon](https://lh3.googleusercontent.com/D84xHaJPyz9ONahHrZywlrnbVsG7lck4XivdcGkxn5Drv2Hb7eAC3ttw20TaH9YhzNY=w360-h155-rw)

![App Screenshot1](https://lh3.googleusercontent.com/GTpkPjnFx3_gNKbR4e_4woarGWy6b0Zs7lJbJPblfGzqT73EfoNHWYUNCDVrNJC7KIE)
![App Screenshot2](https://lh3.googleusercontent.com/vGLRLZONZ1584MMlHTjOPEN4eZQ91Fvj-fr5ApND-yWWnsUw70iBMqeh1obpl3cXLQ)
![App Screenshot3](https://lh3.googleusercontent.com/PwOfK1b1sgRq2YkAhiI2HIlqwA_4_WR0KplkkhpQ5YoHUDY8E-7XB-aan_dPhjHeAuI)

![App Screenshot4](https://lh3.googleusercontent.com/yVA9YHJ0FMk2injRlJ-cE0lhtaKX8yC3HkRvh1ucbmKa8c-d947tiQ1VDgO90fkYBg)
![App Screenshot5](https://lh3.googleusercontent.com/K14qmJC76VWQtML-iZGGOU_cQen8oxHFtcjBDyUexesat_HNG6AceBipfXJis7i8IUru)

### Version History ###

#### Version 1.0.0 (1) - 2021.02.18 - It searches Reddit, displays results in RecyclerView, and opens details in a CustomTab ####
* Reverse engineered and simplified Android sample app to learn Android Jetpack
    * https://github.com/android/architecture-components-samples/tree/master/PagingWithNetworkSample
        * simplified to use "Paging Using Next Tokens From The Previous Query" 
            * This sample, implemented in the InMemoryByPageKeyRepository class, demonstrates how to utilize the before and after keys in the response to discover the next page.
    
* Andorid Jetpack - Architecture Components
	* https://developer.android.com/topic/libraries/architecture
	
    * lifecycle.ViewModel 
        * https://developer.android.com/reference/kotlin/androidx/lifecycle/ViewModel
            * ViewModel is a class that is responsible for preparing and managing the data for an Activity or a Fragment.
            * The purpose of the ViewModel is to acquire and keep the information that is necessary for an Activity or a Fragment.
            * ViewModel's only responsibility is to manage the data for the UI. It should never access your view hierarchy or hold a reference back to the Activity or the Fragment.
        * https://developer.android.com/topic/libraries/architecture/viewmodel
            * The ViewModel class is designed to store and manage UI-related data in a lifecycle conscious way. 
            * The ViewModel class allows data to survive configuration changes such as screen rotations. 
        * Troubleshoot: this class can only be used with the compiler argument '-Xopt-in=kotlin.RequiresOptIn'
            * https://discuss.kotlinlang.org/t/how-to-supress-optin-warnings-in-gradle-and-during-git-commit-code-analysis/17981
            
    * lifecycle.SavedStateHandle https://developer.android.com/reference/kotlin/androidx/lifecycle/SavedStateHandle
        * A handle to saved state passed down to androidx.lifecycle.ViewModel
        
        
* use Kotlin coroutines with Architecture components
	* https://developer.android.com/topic/libraries/architecture/coroutines
    * https://developer.android.com/kotlin/coroutines
        * A coroutine is a concurrency design pattern that you can use on Android to simplify code that executes asynchronously.
        * On Android, coroutines help to manage long-running tasks that might otherwise block the main thread and cause your app to become unresponsive. Over 50% of professional developers who use coroutines have reported seeing increased productivity. 
	* https://developer.android.com/kotlin/flow
        * In coroutines, a flow is a type that can emit multiple values sequentially, as opposed to suspend functions that return only a single value. For example, you can use a flow to receive live updates from a database.
        * Flows are built on top of coroutines and can provide multiple values. A flow is conceptually a stream of data that can be computed asynchronously. The emitted values must be of the same type. For example, a Flow<Int> is a flow that emits integer values.
    * https://kotlin.github.io/kotlinx.coroutines/kotlinx-coroutines-core/kotlinx.coroutines.channels/-channel/
        * Channel is a non-blocking primitive for communication between a sender (via SendChannel) and a receiver (via ReceiveChannel). Conceptually, a channel is similar to Java’s BlockingQueue, but it has suspending operations instead of blocking ones and can be closed.

* use kapt, the Kotlin annotation processing tool - for value objects
    * https://kotlinlang.org/docs/kapt.html#generating-kotlin-sources
    * https://levelup.gitconnected.com/a-look-into-kotlin-kapt-for-android-code-in-mobile-46ca10d8cb63


* use Paging library
    * https://developer.android.com/topic/libraries/architecture/paging
        * The Paging Library helps you load and display small chunks of data at a time. Loading partial data on demand reduces usage of network bandwidth and system resources.
    * https://developer.android.com/reference/kotlin/androidx/paging/PagingData
        * Container for Paged data from a single generation of loads.  Each refresh of data (generally either pushed by local storage, or pulled from the network) will have a separate corresponding PagingData.
    * Load and display paged data
        * https://developer.android.com/topic/libraries/architecture/paging/v3-paged-data       
            * loadStateFlow.collectLatest
            * loadStateFlow.distinctUntilChangedBy
            * loadStateFlow.filter
            * Troubleshoot: https://stackoverflow.com/questions/62347539/type-mismatch-inferred-type-is-unit-but-flowcollectorint-was-expected
                * collect gives the error: Type mismatch: inferred type is () -> Unit but FlowCollector<Int> was expected
                * For your syntax, you need to import the collect() extension function.
                * import kotlinx.coroutines.flow.collect


* save data in a local database using Room   
    * https://developer.android.com/training/data-storage/room

* use Retrofit & OkHttp
    * Retrofit https://square.github.io/retrofit/
        * A type-safe HTTP client for Android and Java 
    * https://square.github.io/okhttp/4.x/okhttp-logging-interceptor/okhttp3.logging/-http-logging-interceptor/
        * An OkHttp interceptor which logs request and response information. Can be applied as an application interceptor or as a OkHttpClient.networkInterceptors.


* use Glide https://github.com/bumptech/glide / http://bumptech.github.io/glide/
    * Glide is a fast and efficient open source media management and image loading framework for Android that wraps media decoding, memory and disk caching, and resource pooling into a simple and easy to use interface.
    * Glide supports fetching, decoding, and displaying video stills, images, and animated GIFs. Glide includes a flexible API that allows developers to plug in to almost any network stack.
    * troubleshoot: import app.demo.apicallerdemo.api.GlideRequests unresolved https://github.com/bumptech/glide/issues/1945
         * added RedditAppGlideModule in package app.demo.apicallerdemo.api
         * in build.gradle use kapt instead of annotationProcessor  kapt 'com.github.bumptech.glide:compiler:4.x.x'
            * implementation 'com.github.bumptech.glide:glide:4.12.0'
            * kapt 'com.github.bumptech.glide:compiler:4.12.0'


* use AndroidX UI 
    * SwipeRefreshLayout https://material.io/design/platform-guidance/android-swipe-to-refresh.html
    * RecyclerView https://developer.android.com/jetpack/androidx/releases/recyclerview


#### Version 1.0.0 (2) - 2021.02.19 - Disaster recovery from crashes on prod ####

* unpublish app to prevent new user from receiving bad build on prod
    * https://support.google.com/googleplay/android-developer/answer/9859350
        * When you unpublish an app, existing users can still use your app and receive app updates. Your app won’t be available for new users to find and download on Google Play.

* make sure to test app build with minifyEnabled true (proguard rules enabled)
    * https://developer.android.com/studio/build/shrink-code

* Publish your app to a closed or open testing track to generate pre-launch reports
    * Pre-launch reports are automatically generated when you publish an app to closed or open testing. The pre-launch report helps you to anticipate and fix issues in your app before they reach your users - including stability, performance, and accessibility issues, security vulnerabilities and privacy concerns.

#### Version 1.0.0 (3) - 2021.02.19 - Fix open source credit issue and added Espresso ####

* Including Open Source Notices https://developers.google.com/android/guides/opensource

* Espresso https://developer.android.com/training/testing/espresso
    * Robot Pattern https://medium.com/android-bits/espresso-robot-pattern-in-kotlin-fc820ce250f7
    * https://bitbucket.org/mhhc/apicallerdemo/src/master/app/src/androidTest/java/app/demo/apicallerdemo/

### pending release ###

* added app bar menu to switch result layout in grid view or list view

* added Android Hilt for dependency injection to replace ServiceLocator
    * https://developer.android.com/codelabs/android-hilt#6
    * https://medium.com/swlh/inject-viewmodel-using-hilt-2c968f1e85fe

* added Android Jetpack Navigation Component for Text-to-Dog-Phone search Activities & Fragments (2021.04.24)
    * https://developer.android.com/guide/navigation
    * https://developer.android.com/codelabs/basic-android-kotlin-training-fragments-navigation-component

* modified codelab to Load and display dog images from the Internet with Retrofit, Moshi, Data Binding, and Coli (2021.04.25)
    * https://developer.android.com/codelabs/basic-android-kotlin-training-internet-images
    
    * use Moshi JSON parser
        * https://github.com/square/moshi

    * use Jetpack Data Binding Library
        * https://developer.android.com/topic/libraries/data-binding
        
    * use Coli - image loading library for Android backed by Kotlin Coroutines
        * https://coil-kt.github.io/coil/

    * use open source API to get dog photos
        * https://dog.ceo/dog-api/

![dog1 screenshot](https://bitbucket.org/mhhc/apicallerdemo/raw/0c6050ac0b1c128cf1346f2e85121749e91dc3ec/screenshots/Screenshot_1619400794.png)

![dog2 screenshot](https://bitbucket.org/mhhc/apicallerdemo/raw/0c6050ac0b1c128cf1346f2e85121749e91dc3ec/screenshots/Screenshot_1619400843.png)

* implemented Espresso IdlingResource to wait until view is displayed
    * https://developer.android.com/training/testing/espresso/idling-resource
    * https://stackoverflow.com/questions/50628219/is-it-possible-to-use-espressos-idlingresource-to-wait-until-a-certain-view-app/50631817

* added local unit test with mockito to mock static methods
    * https://developer.android.com/training/testing/unit-testing/local-unit-tests#mocking-dependencies
    * https://rieckpil.de/mocking-static-methods-with-mockito-java-kotlin/

### Future Learning ###

* explore more on other samples https://github.com/android/architecture-components-samples/tree/master
	
* integrate crash reporting 
    * https://firebase.google.com/docs/crashlytics	


### Reference ###

* https://developer.android.com/jetpack
* https://github.com/android/architecture-components-samples/tree/master/PagingWithNetworkSample
* https://www.raywenderlich.com/12244218-paging-library-for-android-with-kotlin-creating-infinite-lists
* [Learn Markdown](https://bitbucket.org/tutorials/markdowndemo)

* Kotlin scope functions
    * https://medium.com/@hitanshudhawan/kotlin-scope-functions-77746c9e66a0
    * https://blog.kotlin-academy.com/mastering-kotlin-scoped-and-higher-order-functions-23e2dd34d660
    
    
