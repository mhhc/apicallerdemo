package app.demo.apicallerdemo.ui.photosearch

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import app.demo.apicallerdemo.databinding.FragmentPhotoSearchDetailBinding

// the fragment initialization parameters
private const val ARG_PHOTO_URL = "photoURL"

/**
 * A simple [Fragment] subclass.
 * Use the [PhotoSearchDetailFragment.newInstance] factory method to
 * create an instance of this fragment.
 */
class PhotoSearchDetailFragment : Fragment() {

    lateinit var binding: FragmentPhotoSearchDetailBinding
        private set

    private var photoURL: String? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        arguments?.let {
            photoURL = it.getString(ARG_PHOTO_URL, "")
        }
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        binding = FragmentPhotoSearchDetailBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onViewCreated(
            view: View,
            savedInstanceState: Bundle?
    ) {
        super.onViewCreated(view, savedInstanceState)
        initPhotoSearchDetail()
    }

    private fun initPhotoSearchDetail() {
        binding.photoSearchDetailTitle.text = photoURL
    }

    companion object {
        /**
         * Use this factory method to create a new instance of
         * this fragment using the provided parameters.
         *
         * @param photoURL Parameter 1.
         * @return A new instance of fragment PhotoSearchFragment.
         */
        @JvmStatic
        fun newInstance(photoURL: String) =
            PhotoSearchDetailFragment().apply {
                arguments = Bundle().apply {
                    putString(ARG_PHOTO_URL, photoURL)
                }
            }
    }
}