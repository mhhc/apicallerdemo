package app.demo.apicallerdemo.ui.photosearch

import android.util.Log
import android.webkit.URLUtil
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import app.demo.apicallerdemo.api.dogphoto.DogPhotoApi
import app.demo.apicallerdemo.api.dogphoto.model.DogPhoto
import app.demo.apicallerdemo.api.dogphoto.model.DogPhotoResult
import kotlinx.coroutines.launch
import java.lang.Exception


enum class ApiStatus {
    LOADING, FAIL, SUCCESS
}


private const val TAG = "PhotoSearchViewModel"

class PhotoSearchViewModel : ViewModel() {

    // The internal MutableLiveData that stores the status of the most recent request
    private val mApiStatus = MutableLiveData<ApiStatus>()
    val apiStatus: LiveData<ApiStatus> = mApiStatus

    // Internally, we use a MutableLiveData, because we will be updating the List of DogPhoto with new values
    private val mDogPhotos = MutableLiveData<List<DogPhoto>>()
    val dogPhotos: LiveData<List<DogPhoto>> = mDogPhotos

    fun getRandomDogPhotos(number: Int) {
        // https://developer.android.com/topic/libraries/architecture/coroutines
        // Any coroutine launched in this scope is automatically canceled if the ViewModel is cleared.
        // Coroutines are useful here for when you have work that needs to be done only if the ViewModel is active.
        viewModelScope.launch {
            mApiStatus.value = ApiStatus.LOADING
            try {
                val dogPhotoResult = DogPhotoApi.retrofitService.getRandomDogPhotos(number)
                val resultStatus = dogPhotoResult.resultStatus
                val resultStatusMessage = "getRandomDogPhotos status: $resultStatus"
                Log.d(TAG, resultStatusMessage)
                if (resultStatus == "success") {
                    mDogPhotos.value = buildDogPhotoList(dogPhotoResult)
                    mApiStatus.value = ApiStatus.SUCCESS
                } else {
                    handleFailedResult(resultStatusMessage)
                }
            } catch (e: Exception) {
                handleFailedResult(e.localizedMessage.orEmpty())
            }
        }
    }

    fun isLoading(): Boolean {
        return apiStatus.equals(ApiStatus.LOADING)
    }

    private fun handleFailedResult(message: String) {
        Log.e(TAG, "getRandomDogPhotos failed $message")
        mDogPhotos.value = listOf()
        mApiStatus.value = ApiStatus.FAIL
    }

    companion object {
        internal fun buildDogPhotoList(dogPhotoResult: DogPhotoResult): List<DogPhoto> {
            val photoList = mutableListOf<DogPhoto>()
            for (photoUrl in dogPhotoResult.photoUrlList) {
                if (URLUtil.isValidUrl(photoUrl)) {
                    photoList.add(DogPhoto(photoUrl))
                }
            }
            return photoList
        }
    }
}