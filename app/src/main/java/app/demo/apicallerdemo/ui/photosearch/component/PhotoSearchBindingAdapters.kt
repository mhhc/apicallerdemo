package app.demo.apicallerdemo.ui.photosearch.component

import android.widget.ImageView
import androidx.core.net.toUri
import androidx.databinding.BindingAdapter
import androidx.recyclerview.widget.RecyclerView
import app.demo.apicallerdemo.R
import app.demo.apicallerdemo.api.dogphoto.model.DogPhoto
import coil.load


/**
 * Updates the data shown in the [RecyclerView].
 */
@BindingAdapter("photosData")
fun bindRecyclerView(recyclerView: RecyclerView, data: List<DogPhoto>?) {
    val adapter = recyclerView.adapter as PhotoSearchRecyclerViewAdapter
    adapter.submitList(data)
}

/**
 * Uses the Coil library to load an image by URL into an [ImageView]
 */
@BindingAdapter("imageUrl")
fun bindImage(imgView: ImageView, imgUrl: String?) {
    imgUrl?.let {
        val imgUri = imgUrl.toUri().buildUpon().scheme("https").build()
        imgView.load(imgUri) {
            placeholder(R.drawable.loading_animation)
            error(R.drawable.ic_baseline_grid_view_24)
        }
    }
}

