package app.demo.apicallerdemo.ui.photosearch

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.navigation.fragment.findNavController
import app.demo.apicallerdemo.R
import app.demo.apicallerdemo.databinding.FragmentPhotoSearchResultBinding
import app.demo.apicallerdemo.ui.photosearch.component.PhotoSearchRecyclerViewAdapter

// the fragment initialization parameters
private const val ARG_PHOTO_SEARCH_TEXT = "photoSearchText"

/**
 * A simple [Fragment] subclass.
 * Use the [PhotoSearchResultFragment.newInstance] factory method to
 * create an instance of this fragment.
 */
class PhotoSearchResultFragment : Fragment() {

    lateinit var binding: FragmentPhotoSearchResultBinding
        private set

    // NOTE: RecyclerView is set with data binding
//    private lateinit var photoSearchResultRecyclerView: RecyclerView

    private var photoSearchText: String? = null

    private val photoSearchViewModel: PhotoSearchViewModel by viewModels()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        arguments?.let {
            photoSearchText = it.getString(ARG_PHOTO_SEARCH_TEXT)
        }
    }

    /**
     * Inflates the layout with Data Binding, sets its lifecycle owner to the PhotoSearchResultFragment
     * to enable Data Binding to observe LiveData, and sets up the RecyclerView with an adapter.
     */
    override fun onCreateView(
            inflater: LayoutInflater, container: ViewGroup?,
            savedInstanceState: Bundle?
    ): View {
        binding = FragmentPhotoSearchResultBinding.inflate(inflater, container, false)

        // Allows Data Binding to Observe LiveData with the lifecycle of this Fragment
        binding.lifecycleOwner = this

        // Giving the binding access to the PhotoSearchViewModel
        binding.photoSearchViewModel = photoSearchViewModel

        // Sets the adapter of the photosGrid RecyclerView
        binding.photoSearchResultRecyclerView.adapter = PhotoSearchRecyclerViewAdapter()

        return binding.root
    }

    override fun onViewCreated(
            view: View,
            savedInstanceState: Bundle?
    ) {
        super.onViewCreated(view, savedInstanceState)
        initPhotoSearchResult()
    }

    private fun initPhotoSearchResult() {
        val searchTitle = resources.getString(R.string.photo_result_title, photoSearchText.orEmpty())
        binding.photoSearchText.text = searchTitle
        binding.photoSearchText.setOnClickListener {
            // TODO: handle photo search detail fragment
            val action = PhotoSearchResultFragmentDirections
                    .actionPhotoSearchResultFragmentToPhotoSearchDetailFragment(
                        photoURL = "https://dog.ceo/dog-api/documentation/random")
            findNavController().navigate(action)
        }

        // NOTE: RecyclerView is set with data binding
//        photoSearchResultRecyclerView = binding.photoSearchResultRecyclerView
//        photoSearchResultRecyclerView.layoutManager = LinearLayoutManager(requireContext())
//        photoSearchResultRecyclerView.addItemDecoration(
//            DividerItemDecoration(context, DividerItemDecoration.VERTICAL)
//        )

        binding.photoSearchViewModel?.getRandomDogPhotos(photoSearchText?.length ?: 1)
    }

    companion object {
        /**
         * Use this factory method to create a new instance of
         * this fragment using the provided parameters.
         *
         * @param photoSearchText Parameter 1.
         * @return A new instance of fragment PhotoSearchResultFragment.
         */
        @JvmStatic
        fun newInstance(photoSearchText: String) =
            PhotoSearchResultFragment().apply {
                arguments = Bundle().apply {
                    putString(ARG_PHOTO_SEARCH_TEXT, photoSearchText)
                }
            }

        fun getBundle(photoSearchText: String): Bundle {
            val bundle = Bundle()
            bundle.putString(ARG_PHOTO_SEARCH_TEXT, photoSearchText)
            return bundle
        }
    }
}