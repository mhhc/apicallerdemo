package app.demo.apicallerdemo.ui.redditsearch

import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.view.Menu
import android.view.MenuItem
import androidx.activity.viewModels
import androidx.appcompat.app.AppCompatActivity
import androidx.core.content.ContextCompat
import androidx.lifecycle.lifecycleScope
import androidx.paging.LoadState
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.LinearLayoutManager
import app.demo.apicallerdemo.R
import app.demo.apicallerdemo.api.reddit.GlideApp
import app.demo.apicallerdemo.databinding.ActivityRedditSearchResultBinding
import app.demo.apicallerdemo.ui.redditsearch.recycerview.PostsAdapter
import app.demo.apicallerdemo.ui.redditsearch.recycerview.PostsLoadStateAdapter
import app.demo.apicallerdemo.util.getBuildVersionCode
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.coroutines.flow.collect
import kotlinx.coroutines.flow.collectLatest
import kotlinx.coroutines.flow.distinctUntilChangedBy
import kotlinx.coroutines.flow.filter

@AndroidEntryPoint
class RedditSearchResultActivity : AppCompatActivity() {

    lateinit var binding: ActivityRedditSearchResultBinding
        private set

    private var isLinearLayoutManager = true

    private val model by viewModels<RedditViewModel>()

    /* NOTE: ServiceLocator is replaced by Hilt.
    private val model: RedditViewModel by viewModels {
        object : AbstractSavedStateViewModelFactory(this, null) {
            override fun <T : ViewModel?> create(
                    key: String,
                    modelClass: Class<T>,
                    handle: SavedStateHandle
            ): T {
                val repo = ServiceLocator.instance(this@RedditSearchResultActivity).getRepository()

                @Suppress("UNCHECKED_CAST")
                return RedditViewModel(repo, handle) as T
            }
        }
    }
     */

    private lateinit var adapter: PostsAdapter

    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        menuInflater.inflate(R.menu.layout_menu, menu)
        val layoutButton = menu?.findItem(R.id.action_switch_layout)
        setIcon(layoutButton)
        return true
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        return when (item.itemId) {
            R.id.action_switch_layout -> {
                isLinearLayoutManager = !isLinearLayoutManager
                chooseLayoutManager()
                setIcon(item)
                return true
            }
            else -> {
                super.onOptionsItemSelected(item)
            }
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityRedditSearchResultBinding.inflate(layoutInflater)
        setContentView(binding.root)
        supportActionBar?.title = getBuildVersionCode()
        initAdapter()
        initSwipeToRefreshResultLayout()
        initSearch()
    }

    private fun initAdapter() {
        val glide = GlideApp.with(this)
        adapter = PostsAdapter(glide)
        chooseLayoutManager()
        binding.redditSearchResultRecyclerView.adapter = adapter.withLoadStateHeaderAndFooter(
            header = PostsLoadStateAdapter(adapter),
            footer = PostsLoadStateAdapter(adapter)
        )

        // https://developer.android.com/topic/libraries/architecture/coroutines
        // A LifecycleScope is defined for each Lifecycle object. Any coroutine launched in this scope is canceled when the Lifecycle is destroyed.
        lifecycleScope.launchWhenCreated {
            adapter.loadStateFlow.collectLatest { loadStates ->
                binding.redditSearchResultSwipeRefreshLayout.isRefreshing = loadStates.refresh is LoadState.Loading
            }
        }

        lifecycleScope.launchWhenCreated {
            model.posts.collectLatest {
                adapter.submitData(it)
            }
        }

        lifecycleScope.launchWhenCreated {
            adapter.loadStateFlow
                // Only emit when REFRESH LoadState for RemoteMediator changes.
                .distinctUntilChangedBy { it.refresh }
                // Only react to cases where Remote REFRESH completes i.e., NotLoading.
                .filter { it.refresh is LoadState.NotLoading }
                // https://stackoverflow.com/questions/62347539/type-mismatch-inferred-type-is-unit-but-flowcollectorint-was-expected
                .collect { binding.redditSearchResultRecyclerView.scrollToPosition(0) }
        }
    }

    private fun chooseLayoutManager() {
        if (isLinearLayoutManager) {
            binding.redditSearchResultRecyclerView.layoutManager = LinearLayoutManager(this)
        } else {
            binding.redditSearchResultRecyclerView.layoutManager = GridLayoutManager(this, 2)
        }
    }

    private fun setIcon(menuItem: MenuItem?) {
        if (menuItem == null) {
            return
        }
        menuItem.icon = if (isLinearLayoutManager) {
            ContextCompat.getDrawable(this, R.drawable.ic_baseline_grid_view_24)
        } else {
            ContextCompat.getDrawable(this, R.drawable.ic_baseline_view_list_24)
        }
    }

    private fun initSwipeToRefreshResultLayout() {
        binding.redditSearchResultSwipeRefreshLayout.setOnRefreshListener {
            adapter.refresh()
        }
    }

    private fun initSearch() {
        val searchTerm = intent?.getStringExtra(KEY_SEARCH_TERM).orEmpty()
        updateRedditSearchResult(searchTerm)
    }

    private fun updateRedditSearchResult(searchTerm: String?) {
        binding.redditSearchTerm.text = getString(R.string.search_result_title, searchTerm)
        searchTerm?.trim().toString().let {
            if (it.isNotBlank() && model.shouldShowSubreddit(it)) {
                model.showSubreddit(it)
            }
        }
    }

    companion object {
        const val KEY_SEARCH_TERM = "key_search_term"

        fun getLaunchIntent(context: Context, searchTerm: String?): Intent {
            val intent = Intent(context, RedditSearchResultActivity::class.java)
            intent.putExtra(KEY_SEARCH_TERM, searchTerm)
            return intent
        }
    }
}