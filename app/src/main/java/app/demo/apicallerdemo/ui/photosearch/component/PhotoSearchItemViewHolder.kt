package app.demo.apicallerdemo.ui.photosearch.component

import androidx.recyclerview.widget.RecyclerView
import app.demo.apicallerdemo.api.dogphoto.model.DogPhoto
import app.demo.apicallerdemo.databinding.ViewPhotoSearchItemBinding

class PhotoSearchItemViewHolder(
    private var binding: ViewPhotoSearchItemBinding
) : RecyclerView.ViewHolder(binding.root) {

    fun bind(dogPhoto: DogPhoto) {
        binding.dogPhoto = dogPhoto

        // This is important, because it forces the data binding to execute immediately,
        // which allows the RecyclerView to make the correct view size measurements
        binding.executePendingBindings()
    }
}
