package app.demo.apicallerdemo.ui.photosearch

import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.view.LayoutInflater
import androidx.appcompat.app.AppCompatActivity
import androidx.navigation.NavController
import androidx.navigation.fragment.NavHostFragment
import androidx.navigation.ui.setupActionBarWithNavController
import app.demo.apicallerdemo.R
import app.demo.apicallerdemo.databinding.ActivityPhotoSearchBinding

class PhotoSearchActivity : AppCompatActivity() {

    private lateinit var binding: ActivityPhotoSearchBinding
    private lateinit var navController: NavController

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        binding = ActivityPhotoSearchBinding.inflate(LayoutInflater.from(this))
        setContentView(binding.root)

        initNavigationFragment()
    }

    private fun initNavigationFragment() {
        val navHostFragment = supportFragmentManager
                .findFragmentById(R.id.photo_search_nav_host_fragment) as NavHostFragment
        navController = navHostFragment.navController

        // Pass activity destination args to a start destination fragment
        // https://developer.android.com/guide/navigation/navigation-migrate#pass_activity_destination_args_to_a_start_destination_fragment
        val searchText = PhotoSearchResultFragment.getBundle(intent?.getStringExtra(KEY_SEARCH_TEXT).orEmpty())
        navController.setGraph(R.navigation.photo_search_nav_graph, searchText)

        setupActionBarWithNavController(navController)
    }

    override fun onSupportNavigateUp(): Boolean {
        return navController.navigateUp() || super.onSupportNavigateUp()
    }

    companion object {
        private const val KEY_SEARCH_TEXT = "key_search_text"

        fun getLaunchIntent(context: Context, searchText: String?): Intent {
            val intent = Intent(context, PhotoSearchActivity::class.java)
            intent.putExtra(KEY_SEARCH_TEXT, searchText)
            return intent
        }
    }
}