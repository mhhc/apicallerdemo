package app.demo.apicallerdemo.ui.redditsearch

import androidx.lifecycle.SavedStateHandle
import androidx.lifecycle.ViewModel
import androidx.lifecycle.asFlow
import androidx.lifecycle.viewModelScope
import androidx.paging.PagingData
import androidx.paging.cachedIn
import app.demo.apicallerdemo.repository.RedditPostRepository
import app.demo.apicallerdemo.repository.model.RedditPost
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.FlowPreview
import kotlinx.coroutines.channels.Channel
import kotlinx.coroutines.flow.*
import javax.inject.Inject

@HiltViewModel
class RedditViewModel @Inject constructor(
        private val repository: RedditPostRepository,
        private val savedStateHandle: SavedStateHandle
) : ViewModel() {

    init {
        if (!savedStateHandle.contains(KEY_SUBREDDIT)) {
            savedStateHandle.set(KEY_SUBREDDIT, DEFAULT_SUBREDDIT)
        }
    }

    // Channel is a non-blocking primitive for communication between a sender (via SendChannel) and a receiver (via ReceiveChannel).
    // Conceptually, a channel is similar to Java’s BlockingQueue, but it has suspending operations instead of blocking ones and can be closed.
    // TODO: read https://kotlin.github.io/kotlinx.coroutines/kotlinx-coroutines-core/kotlinx.coroutines.channels/-channel/
    private val clearListCh = Channel<Unit>(Channel.CONFLATED)

    // Troubleshoot: this class can only be used with the compiler argument '-Xopt-in=kotlin.RequiresOptIn'
    // https://discuss.kotlinlang.org/t/how-to-supress-optin-warnings-in-gradle-and-during-git-commit-code-analysis/17981
    @OptIn(ExperimentalCoroutinesApi::class, FlowPreview::class)
    val posts = flowOf(
            clearListCh.receiveAsFlow().map { PagingData.empty<RedditPost>() },
            savedStateHandle.getLiveData<String>(KEY_SUBREDDIT)
                    .asFlow()
                    .flatMapLatest { repository.postsOfSubreddit(it, 30) }
                    // cachedIn() shares the paging state across multiple consumers of posts,
                    // e.g. different generations of UI across rotation config change
                    .cachedIn(viewModelScope)
    ).flattenMerge(2)

    fun shouldShowSubreddit(subreddit: String) = savedStateHandle.get<String>(KEY_SUBREDDIT) != subreddit

    fun showSubreddit(subreddit: String) {
        if (!shouldShowSubreddit(subreddit)) return
        clearListCh.offer(Unit)
        savedStateHandle.set(KEY_SUBREDDIT, subreddit)
    }

    companion object {
        const val KEY_SUBREDDIT = "subreddit"
        const val DEFAULT_SUBREDDIT = "androiddev"
    }
}