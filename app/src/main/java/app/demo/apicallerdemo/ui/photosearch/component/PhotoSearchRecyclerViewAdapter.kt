package app.demo.apicallerdemo.ui.photosearch.component

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView
import app.demo.apicallerdemo.api.dogphoto.model.DogPhoto
import app.demo.apicallerdemo.databinding.ViewPhotoSearchItemBinding


class PhotoSearchRecyclerViewAdapter : ListAdapter<DogPhoto, PhotoSearchItemViewHolder>(DiffCallback) {

    /**
     * Allows the RecyclerView to determine which items have changed when the [List] of
     * [DogPhoto] has been updated.
     */
    companion object DiffCallback : DiffUtil.ItemCallback<DogPhoto>() {
        override fun areItemsTheSame(oldItem: DogPhoto, newItem: DogPhoto): Boolean {
            return oldItem.imageURL == newItem.imageURL
        }

        override fun areContentsTheSame(oldItem: DogPhoto, newItem: DogPhoto): Boolean {
            return oldItem.imageURL == newItem.imageURL
        }
    }

    /**
     * Create new [RecyclerView] item views (invoked by the layout manager)
     */
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): PhotoSearchItemViewHolder {
        return PhotoSearchItemViewHolder(
            ViewPhotoSearchItemBinding.inflate(LayoutInflater.from(parent.context))
        )
    }

    /**
     * Replaces the contents of a view (invoked by the layout manager)
     */
    override fun onBindViewHolder(holder: PhotoSearchItemViewHolder, position: Int) {
        val photo = getItem(position)
        holder.bind(photo)
    }
}
