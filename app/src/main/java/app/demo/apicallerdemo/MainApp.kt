package app.demo.apicallerdemo

import android.app.Application
import android.content.res.Resources
import dagger.hilt.android.HiltAndroidApp

// https://antonioleiva.com/objects-kotlin/
@HiltAndroidApp
class MainApp : Application() {

    override fun onCreate() {
        super.onCreate()
        instance = this
        res = this.resources
    }

    companion object {
        lateinit var instance: MainApp
            private set

        lateinit var res: Resources
            private set
    }

}