package app.demo.apicallerdemo.di

import app.demo.apicallerdemo.repository.InMemoryByPageKeyRepository
import app.demo.apicallerdemo.repository.RedditPostRepository
import dagger.Binds
import dagger.Module
import dagger.hilt.InstallIn
import dagger.hilt.android.components.ViewModelComponent

/**
 * https://developer.android.com/codelabs/android-hilt#6
 * https://medium.com/swlh/inject-viewmodel-using-hilt-2c968f1e85fe
 */
@Module
@InstallIn(ViewModelComponent::class)
abstract class RepositoryModule {

    @Binds
    abstract fun providesInMemoryByPageKeyRepository(repo: InMemoryByPageKeyRepository): RedditPostRepository

}