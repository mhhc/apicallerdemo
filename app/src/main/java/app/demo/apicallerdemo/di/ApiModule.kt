package app.demo.apicallerdemo.di

import app.demo.apicallerdemo.api.reddit.RedditApi
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.components.SingletonComponent

@Module
@InstallIn(SingletonComponent::class)
object ApiModule {

    @Provides
    fun providesRedditApi(): RedditApi {
        return RedditApi.create()
    }

}