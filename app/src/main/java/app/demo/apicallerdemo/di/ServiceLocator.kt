package app.demo.apicallerdemo.di

/**
 * NOTE: This ServiceLocator is commented out for reference
 * and is replaced by using Android Hilt dependency injection.
 * https://developer.android.com/training/dependency-injection/hilt-android
 *
 * Super simplified service locator implementation to allow us to replace default implementations
 * for testing.
 *
 * https://en.wikipedia.org/wiki/Service_locator_pattern
 * https://developer.android.com/training/dependency-injection#di-alternatives
 * An alternative to dependency injection
 */
/*
interface ServiceLocator {
    companion object {
        private val LOCK = Any()
        private var instance: ServiceLocator? = null

        fun instance(context: Context): ServiceLocator {
            synchronized(LOCK) {
                if (instance == null) {
                    instance = DefaultServiceLocator(
                            app = context.applicationContext as Application,
                            useInMemoryDb = false)
                }
                return instance
            }
        }

        /**
         * Allows tests to replace the default implementations.
         */
        @VisibleForTesting
        fun swap(locator: ServiceLocator) {
            instance = locator
        }
    }

    fun getRepository(): RedditPostRepository

    fun getRedditApi(): RedditApi
}
 */

/**
 * default implementation of ServiceLocator that uses production endpoints.
 */
/*
open class DefaultServiceLocator(val app: Application, val useInMemoryDb: Boolean) : ServiceLocator {

    private val api by lazy {
        RedditApi.create()
    }

    override fun getRepository(): RedditPostRepository {
        return InMemoryByPageKeyRepository(
                redditApi = getRedditApi()
        )
    }

    override fun getRedditApi(): RedditApi = api
}
 */
