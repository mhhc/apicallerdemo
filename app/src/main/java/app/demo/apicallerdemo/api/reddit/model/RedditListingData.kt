package app.demo.apicallerdemo.api.reddit.model

class RedditListingData(
    val children: List<RedditChildrenResponse>,
    val after: String?,
    val before: String?
)
