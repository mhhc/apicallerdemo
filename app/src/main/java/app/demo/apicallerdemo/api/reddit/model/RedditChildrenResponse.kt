package app.demo.apicallerdemo.api.reddit.model

import app.demo.apicallerdemo.repository.model.RedditPost

data class RedditChildrenResponse(val data: RedditPost)