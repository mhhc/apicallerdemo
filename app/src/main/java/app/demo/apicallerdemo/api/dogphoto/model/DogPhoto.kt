package app.demo.apicallerdemo.api.dogphoto.model

data class DogPhoto(
    val imageURL: String,
)
