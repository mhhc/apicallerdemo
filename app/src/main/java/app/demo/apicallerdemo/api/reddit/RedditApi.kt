package app.demo.apicallerdemo.api.reddit

import android.util.Log
import app.demo.apicallerdemo.api.reddit.model.RedditListingResponse
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import retrofit2.http.GET
import retrofit2.http.Path
import retrofit2.http.Query

/**
 * API communication setup
 */
interface RedditApi {

    @GET("/r/{subreddit}/hot.json")
    suspend fun getTop(
            @Path("subreddit") subreddit: String,
            @Query("limit") limit: Int,
            @Query("after") after: String? = null,
            @Query("before") before: String? = null
    ): RedditListingResponse

    companion object {
        private const val BASE_URL = "https://www.reddit.com/"

        fun create(): RedditApi {
            val logger = HttpLoggingInterceptor(HttpLoggingInterceptor.Logger { Log.d("API", it) })
            logger.level = HttpLoggingInterceptor.Level.BASIC

            val client = OkHttpClient.Builder()
                    .addInterceptor(logger)
                    .build()
            return Retrofit.Builder()
                    .baseUrl(BASE_URL)
                    .client(client)
                    .addConverterFactory(GsonConverterFactory.create())
                    .build()
                    .create(RedditApi::class.java)
        }
    }
}