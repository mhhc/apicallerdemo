package app.demo.apicallerdemo.api.dogphoto.model

import com.squareup.moshi.Json

data class DogPhotoResult(
    @Json(name = "message")
    val photoUrlList: List<String>,

    @Json(name = "status")
    val resultStatus: String,
)
