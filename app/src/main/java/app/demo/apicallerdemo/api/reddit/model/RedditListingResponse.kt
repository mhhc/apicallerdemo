package app.demo.apicallerdemo.api.reddit.model

class RedditListingResponse(val data: RedditListingData)
