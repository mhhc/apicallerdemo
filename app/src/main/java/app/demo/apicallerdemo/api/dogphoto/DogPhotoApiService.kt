package app.demo.apicallerdemo.api.dogphoto

import app.demo.apicallerdemo.api.dogphoto.model.DogPhotoResult
import com.squareup.moshi.Moshi
import com.squareup.moshi.kotlin.reflect.KotlinJsonAdapterFactory
import retrofit2.Retrofit
import retrofit2.converter.moshi.MoshiConverterFactory
import retrofit2.http.GET
import retrofit2.http.Path


private const val BASE_URL = "https://dog.ceo/api/"

private val moshi = Moshi.Builder()
    .add(KotlinJsonAdapterFactory())
    .build()

private val retrofit = Retrofit.Builder()
    .addConverterFactory(MoshiConverterFactory.create(moshi))
    .baseUrl(BASE_URL)
    .build()


interface DogPhotoApiService {

    /**
     * Get random dog photos
     * Note: a suspend function, so that you can call this method from within a coroutine.
     */
    @GET("breeds/image/random/{number}")
    suspend fun getRandomDogPhotos(
        @Path(value = "number")
        number: Int
    ): DogPhotoResult
}


object DogPhotoApi {

    /**
     * Note: Remember "lazy instantiation" is when object creation is purposely delayed until you
     * actually need that object to avoid unnecessary computation or use of other computing resources.
     * Kotlin has first-class support for lazy instantiation.
     *
     * Note2: make this lazy initialization, to make sure it is initialized at its first usage
     * https://www.baeldung.com/kotlin/lazy-initialization
     */
    val retrofitService: DogPhotoApiService by lazy {
        retrofit.create(DogPhotoApiService::class.java)
    }
}