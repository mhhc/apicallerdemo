package app.demo.apicallerdemo.util

import android.content.ActivityNotFoundException
import android.content.Context
import android.net.Uri
import android.webkit.URLUtil
import android.widget.Toast
import androidx.browser.customtabs.CustomTabColorSchemeParams
import androidx.browser.customtabs.CustomTabsIntent
import androidx.core.content.ContextCompat
import app.demo.apicallerdemo.BuildConfig
import app.demo.apicallerdemo.MainApp
import app.demo.apicallerdemo.R
import java.net.URLEncoder
import java.nio.charset.StandardCharsets


fun getBuildVersionCode(): String {
    return MainApp.res.getString(R.string.app_name) + " v${BuildConfig.VERSION_NAME}"
}

/*
 * Opens url in custom tab inside the app
 */
fun openCustomTabBrowser(context: Context, url: String) {
    if (!URLUtil.isValidUrl(url)) {
        "https://www.google.com/search?q=${URLEncoder.encode(url, StandardCharsets.UTF_8.name())}"
    }
    val builder = CustomTabsIntent.Builder()
    val params = CustomTabColorSchemeParams.Builder()
            .setNavigationBarColor(ContextCompat.getColor(context, R.color.green))
            .setToolbarColor(ContextCompat.getColor(context, R.color.green))
            .setSecondaryToolbarColor(ContextCompat.getColor(context, R.color.green))
            .build()
    // https://stackoverflow.com/questions/61295936/cant-change-chrome-custom-tabs-navigation-bar-color-when-running-in-the-android/61297124
    builder.setColorSchemeParams(CustomTabsIntent.COLOR_SCHEME_DARK, params)
    builder.setStartAnimations(context, android.R.anim.slide_in_left, android.R.anim.slide_out_right)
    builder.setExitAnimations(context, android.R.anim.slide_in_left,android.R.anim.slide_out_right)

    val customTabsIntent = builder.build()
    try {
        customTabsIntent.launchUrl(context, Uri.parse(url))
    } catch (e: ActivityNotFoundException) {
        Toast.makeText(context, context.resources.getString(R.string.error_open_in_browser), Toast.LENGTH_SHORT).show()
    }
}