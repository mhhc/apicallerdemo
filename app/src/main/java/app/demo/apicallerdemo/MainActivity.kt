package app.demo.apicallerdemo

import android.content.Intent
import android.os.Bundle
import android.view.LayoutInflater
import androidx.appcompat.app.AppCompatActivity
import app.demo.apicallerdemo.databinding.ActivityMainBinding
import app.demo.apicallerdemo.ui.photosearch.PhotoSearchActivity
import app.demo.apicallerdemo.ui.redditsearch.RedditSearchResultActivity
import app.demo.apicallerdemo.util.getBuildVersionCode
import com.google.android.gms.oss.licenses.OssLicensesMenuActivity

class MainActivity : AppCompatActivity() {

    private lateinit var binding: ActivityMainBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityMainBinding.inflate(LayoutInflater.from(this))
        setContentView(binding.root)
        supportActionBar?.title = getBuildVersionCode()

        initPhotoSearchInputLayout()
        initRedditSearchInputLayout()
        initCreditText()
    }

    private fun initPhotoSearchInputLayout() {
        binding.mainPhotoSearchInputLayout.viewApiCallSearchTextInputEditText.setText(R.string.default_search_term)
        binding.mainPhotoSearchInputLayout.viewApiCallSearchTextInputLayout.setHint(R.string.text_to_dog_photos)
        with(binding.mainPhotoSearchInputLayout.viewApiCallSearchButton) {
            setText(R.string.fetch)
            setOnClickListener {
                val searchText = binding.mainPhotoSearchInputLayout.viewApiCallSearchTextInputEditText.text.toString()
                val intent = PhotoSearchActivity.getLaunchIntent(context, searchText)
                startActivity(intent)
            }
        }
    }

    private fun initRedditSearchInputLayout() {
        binding.mainRedditSearchInputLayout.viewApiCallSearchTextInputEditText.setText(R.string.default_search_term)
        binding.mainRedditSearchInputLayout.viewApiCallSearchTextInputLayout.setHint(R.string.search_reddit)
        with(binding.mainRedditSearchInputLayout.viewApiCallSearchButton) {
            setText(R.string.search_reddit)
            setOnClickListener {
                val searchTerm = binding.mainRedditSearchInputLayout.viewApiCallSearchTextInputEditText.text.toString()
                val intent = RedditSearchResultActivity.getLaunchIntent(context, searchTerm)
                startActivity(intent)
            }
        }
    }

    private fun initCreditText() {
        binding.mainCredit.setOnClickListener {
            startActivity(Intent(this, OssLicensesMenuActivity::class.java))
        }
    }
}
