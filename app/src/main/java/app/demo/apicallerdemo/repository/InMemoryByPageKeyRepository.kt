package app.demo.apicallerdemo.repository

import androidx.paging.Pager
import androidx.paging.PagingConfig
import app.demo.apicallerdemo.api.reddit.RedditApi
import javax.inject.Inject
import javax.inject.Singleton

/**
 * Repository implementation that loads data directly from network by using the previous / next page
 * keys returned in the query.
 */
@Singleton
class InMemoryByPageKeyRepository @Inject constructor(private val redditApi: RedditApi) : RedditPostRepository {
    override fun postsOfSubreddit(subReddit: String, pageSize: Int) = Pager(
        PagingConfig(pageSize)
    ) {
        PageKeyedSubredditPagingSource(
            redditApi = redditApi,
            subredditName = subReddit
        )
    }.flow
}
