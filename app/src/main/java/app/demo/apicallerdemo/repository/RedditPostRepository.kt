package app.demo.apicallerdemo.repository

import androidx.paging.PagingData
import app.demo.apicallerdemo.repository.model.RedditPost
import kotlinx.coroutines.flow.Flow

interface RedditPostRepository {
    fun postsOfSubreddit(subReddit: String, pageSize: Int): Flow<PagingData<RedditPost>>
}