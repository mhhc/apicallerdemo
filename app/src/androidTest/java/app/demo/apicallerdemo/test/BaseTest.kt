package app.demo.apicallerdemo.test

import android.content.Context
import android.os.SystemClock
import androidx.test.core.app.ApplicationProvider
import androidx.test.ext.junit.rules.ActivityScenarioRule
import androidx.test.platform.app.InstrumentationRegistry
import app.demo.apicallerdemo.MainActivity
import org.junit.Assert
import org.junit.Before
import org.junit.Rule

open class BaseTest {

    @get:Rule
    val activityRule = ActivityScenarioRule(MainActivity::class.java)

    @Before
    fun beforeTestCheck() {
        val appContext = InstrumentationRegistry.getInstrumentation().targetContext
        Assert.assertTrue("beforeTestCheck packageName is changed.", appContext.packageName.startsWith("app.demo.apicallerdemo"))
    }

    companion object {
        private val appContext:Context = ApplicationProvider.getApplicationContext()

        fun getString(resId: Int): String {
            return appContext.resources.getString(resId)
        }

        fun wait(seconds: Float) = apply {
            SystemClock.sleep(seconds.toLong() * 1000L)
        }
    }
}