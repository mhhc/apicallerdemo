package app.demo.apicallerdemo.test

import androidx.test.ext.junit.runners.AndroidJUnit4
import androidx.test.filters.LargeTest
import app.demo.apicallerdemo.util.mainScreen
import app.demo.apicallerdemo.util.resultScreen
import org.junit.Test
import org.junit.runner.RunWith

@RunWith(AndroidJUnit4::class)
@LargeTest
class MainScreenTest : BaseTest() {

    @Test
    fun testSearchReddit() {
        val searchTerm = "google"

        mainScreen {
            assertOpenSourceNotice()
            inputRedditSearchTerm(searchTerm)
            clickSearchRedditButton()
        }

        resultScreen {
            assertSearchTerm(searchTerm)
        }
    }

}