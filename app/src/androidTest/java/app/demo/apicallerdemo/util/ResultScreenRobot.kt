package app.demo.apicallerdemo.util

import androidx.test.espresso.matcher.ViewMatchers.withId
import androidx.test.espresso.matcher.ViewMatchers.withSubstring
import app.demo.apicallerdemo.R

fun resultScreen(robotMethod: ResultScreenRobot.() -> Unit) =
    ResultScreenRobot().apply { robotMethod() }

class ResultScreenRobot : BaseTestRobot() {

    fun assertSearchTerm(searchTerm: String) {
        checkViewIsVisibleWithText(withId(R.id.reddit_search_term), withSubstring(searchTerm))
    }

}