package app.demo.apicallerdemo.util

import android.view.View
import androidx.test.espresso.Espresso.onView
import androidx.test.espresso.IdlingRegistry
import androidx.test.espresso.IdlingResource
import androidx.test.espresso.ViewInteraction
import androidx.test.espresso.action.ViewActions
import androidx.test.espresso.action.ViewActions.click
import androidx.test.espresso.assertion.ViewAssertions.matches
import androidx.test.espresso.matcher.ViewMatchers.isCompletelyDisplayed
import androidx.test.espresso.matcher.ViewMatchers.isDisplayed
import org.hamcrest.Matcher


open class BaseTestRobot {

    private fun ViewInteraction.waitUntilIsDisplayed(matcher: Matcher<View>): ViewInteraction {
        val idlingResource: IdlingResource = ViewIdlingResource(matcher)
        try {
            IdlingRegistry.getInstance().register(idlingResource)
            onView(matcher).check(matches(isDisplayed()))
        } finally {
            IdlingRegistry.getInstance().unregister(idlingResource)
        }
        return this
    }

    fun checkViewIsVisible(matcher: Matcher<View>): ViewInteraction =
            onView(matcher)
                    .waitUntilIsDisplayed(matcher)
                    .check(matches(isCompletelyDisplayed()))
                    .check(matches(isDisplayed()))

    fun checkViewIsVisibleWithText(viewMatcher: Matcher<View>, textMatcher: Matcher<View>): ViewInteraction =
            onView(viewMatcher)
                    .waitUntilIsDisplayed(viewMatcher)
                    .check(matches(isCompletelyDisplayed()))
                    .check(matches(textMatcher))
                    .check(matches(isDisplayed()))

    fun clickView(matcher: Matcher<View>): ViewInteraction =
            onView(matcher)
                    .waitUntilIsDisplayed(matcher)
                    .check(matches(isCompletelyDisplayed()))
                    .perform(click())

    fun inputTextToView(matcher: Matcher<View>, text: String): ViewInteraction =
            onView(matcher)
                    .waitUntilIsDisplayed(matcher)
                    .check(matches(isCompletelyDisplayed()))
                    .perform(
                            ViewActions.replaceText(text),
                            ViewActions.pressImeActionButton()
                    )
}