package app.demo.apicallerdemo.util

import androidx.test.espresso.matcher.ViewMatchers.*
import app.demo.apicallerdemo.R
import org.hamcrest.Matchers.allOf

// https://medium.com/android-bits/espresso-robot-pattern-in-kotlin-fc820ce250f7
// By using the power of Kotlin we make each fun return the object itself with apply.
// https://medium.com/@hitanshudhawan/kotlin-scope-functions-77746c9e66a0
// https://blog.kotlin-academy.com/mastering-kotlin-scoped-and-higher-order-functions-23e2dd34d660
// uses Extension Functions & Higher-Order Function and Lambda
fun mainScreen(robotMethod: MainScreenRobot.() -> Unit) =
    MainScreenRobot().apply { robotMethod() }

class MainScreenRobot : BaseTestRobot() {

    fun inputRedditSearchTerm(searchTerm: String) {
        inputTextToView(allOf(withId(R.id.view_api_call_search_text_input_edit_text),
                isDescendantOfA(withId(R.id.main_reddit_search_input_layout))), searchTerm)
    }

    fun clickSearchRedditButton() {
        clickView(allOf(withId(R.id.view_api_call_search_button),
                isDescendantOfA(withId(R.id.main_reddit_search_input_layout))))
    }

    fun assertOpenSourceNotice() {
        checkViewIsVisible(withId(R.id.main_credit))
    }

}