package app.demo.apicallerdemo

import android.webkit.URLUtil
import app.demo.apicallerdemo.api.dogphoto.model.DogPhoto
import app.demo.apicallerdemo.api.dogphoto.model.DogPhotoResult
import app.demo.apicallerdemo.ui.photosearch.PhotoSearchViewModel
import org.junit.After
import org.junit.Assert.*
import org.junit.Before
import org.junit.Test
import org.junit.runner.RunWith
import org.mockito.ArgumentMatchers.anyString
import org.mockito.MockedStatic
import org.mockito.Mockito
import org.mockito.junit.MockitoJUnitRunner


/**
 * Local unit test, which will execute on the development machine (host).
 * See [testing documentation](http://d.android.com/tools/testing).
 */
@RunWith(MockitoJUnitRunner::class)
class PhotoSearchViewModelTest {

    private lateinit var mockedURLUtil: MockedStatic<URLUtil>

    @Before
    fun beforeTest() {
        // https://rieckpil.de/mocking-static-methods-with-mockito-java-kotlin/
        mockedURLUtil = Mockito.mockStatic(URLUtil::class.java)
    }

    @After
    fun afterTest() {
        mockedURLUtil.close()
    }

    @Test
    fun buildDogPhotoList_withValidImageURL() {
        mockedURLUtil.let { mocked ->
            mocked.`when`<Any> { URLUtil.isValidUrl(anyString()) }.thenReturn(true)
            assertEquals(true, URLUtil.isValidUrl(""))
        }

        val validURL1 = "https://dog.ceo/api/breeds/image/random"
        val validURL2 = "https://images.dog.ceo/breeds/pekinese/n02086079_20360.jpg"
        run {
            val input = DogPhotoResult(
                    photoUrlList = listOf(validURL1, validURL2),
                    resultStatus = "status"
            )
            val actualResult = PhotoSearchViewModel.buildDogPhotoList(input)
            val expectedResult = listOf(DogPhoto(validURL1), DogPhoto(validURL2))
            assertEquals(expectedResult, actualResult)

            val unexpectedResult = listOf(DogPhoto(validURL2), DogPhoto(validURL1))
            assertNotEquals(unexpectedResult, actualResult)
        }
        run {
            val input = DogPhotoResult(
                    photoUrlList = listOf(validURL1, validURL2, validURL2),
                    resultStatus = ""
            )
            val actualResult = PhotoSearchViewModel.buildDogPhotoList(input)
            val expectedResult = listOf(DogPhoto(validURL1), DogPhoto(validURL2), DogPhoto(validURL2))
            assertEquals(expectedResult, actualResult)

            val unexpectedResult = listOf(DogPhoto(validURL2), DogPhoto(validURL1), DogPhoto(validURL2))
            assertNotEquals(unexpectedResult, actualResult)
        }
    }

    @Test
    fun buildDogPhotoList_withInvalidImageURL() {
        mockedURLUtil.let { mocked ->
            mocked.`when`<Any> { URLUtil.isValidUrl(anyString()) }.thenReturn(false)
            assertEquals(false, URLUtil.isValidUrl(""))
        }

        val invalidURL = "invalidURL"
        run {
            val input = DogPhotoResult(
                    photoUrlList = listOf(invalidURL),
                    resultStatus = "status"
            )
            val actualResult = PhotoSearchViewModel.buildDogPhotoList(input)
            val expectedResult = listOf<DogPhoto>()
            assertEquals(expectedResult, actualResult)

            val unexpectedResult = listOf(DogPhoto(invalidURL))
            assertNotEquals(unexpectedResult, actualResult)
        }
        run {
            val input = DogPhotoResult(
                    photoUrlList = listOf<String>(),
                    resultStatus = ""
            )
            val actualResult = PhotoSearchViewModel.buildDogPhotoList(input)
            val expectedResult = listOf<DogPhoto>()
            assertEquals(expectedResult, actualResult)

            val unexpectedResult = listOf(DogPhoto(""))
            assertNotEquals(unexpectedResult, actualResult)
        }
    }

}